package br.com.fipe.service;

import br.com.fipe.exception.InternalErrorApiException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Service;

import br.com.fipe.model.VeiculoEnum;
import br.com.fipe.util.BufferUtil;

@Service
public class ConsultaFipeService {

	private final String BASE_URL = "http://fipeapi.appspot.com/api/1/";
	private final String GET = "GET";

	/**
	 * Abre a conexão e faz a requisição
	 * @param URL
	 * @param METODO
	 * @return a conexao
	 * @throws Exception
	 */
	private HttpURLConnection abrirConexao(final String URL, final String METODO) throws Exception {
		URL url = new URL(URL);
		HttpURLConnection conexao = null;
		try {
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(METODO);
		}catch (IOException e){
			e.printStackTrace();
			throw new InternalErrorApiException("Erro fatal ao se comunicar com a API externa.");
		}
		return conexao;
	}

	private String consultar(final String URL, final String METODO) throws Exception {
		HttpURLConnection conexao = abrirConexao(URL, METODO);
		String retorno = BufferUtil.convertInputParaString(conexao.getInputStream());
		conexao.disconnect();
		return retorno;

	}

	protected String consultaMarcasPorTipoDeVeiculo(VeiculoEnum veiculo) throws Exception {
		String URL = BASE_URL + veiculo.getDescricao() + "/marcas.json";
		return consultar(URL, GET);
	}

	protected String consultaVeiculosPorMarca(VeiculoEnum veiculoEnum, String marcaId) throws Exception {
		final String URL = BASE_URL + veiculoEnum.getDescricao() + "/veiculos/" + marcaId + ".json";
		return consultar(URL, GET);
	}

	protected String consultaModeloPorVeiculo(VeiculoEnum veiculoEnum, String marcaId, String veiculoId)
			throws Exception {
		final String URL = BASE_URL + veiculoEnum.getDescricao() + "/veiculo/" + marcaId + "/" + veiculoId + ".json ";
		return consultar(URL, GET);
	}

	protected String consultaPrecoVeiculo(VeiculoEnum veiculoEnum, String marcaId, String veiculoId, String key)
			throws Exception {
		final String URL = BASE_URL + veiculoEnum.getDescricao() + "/veiculo/" + marcaId + "/" + veiculoId + "/" + key + ".json ";
		return consultar(URL, GET);
	}

}
