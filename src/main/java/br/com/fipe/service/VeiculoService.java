package br.com.fipe.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.fipe.controller.dto.MarcaDTO;
import br.com.fipe.controller.dto.ModeloDTO;
import br.com.fipe.controller.dto.VeiculoDTO;
import br.com.fipe.controller.dto.VeiculoPrecoDTO;
import br.com.fipe.model.MarcaModel;
import br.com.fipe.model.ModeloModel;
import br.com.fipe.model.VeiculoModel;
import br.com.fipe.model.VeiculoEnum;
import br.com.fipe.util.JsonFactoryUtil;

public abstract class VeiculoService {

	protected abstract VeiculoEnum getTipoVeiculo();

	@Autowired
	private ConsultaFipeService consultaFipeService;

	/**
	 * Busca todas as marcas de carros, ordenando pelo nome
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<MarcaDTO> buscarMarcas() throws Exception {
		final String MARCAS = consultaFipeService.consultaMarcasPorTipoDeVeiculo(getTipoVeiculo());
		return Arrays.asList(JsonFactoryUtil.criarObjetos(MARCAS, MarcaModel.class)).stream()
				.sorted(Comparator.comparing(MarcaModel::getName)).map(this::converter).collect(Collectors.toList());

	}

	public List<VeiculoDTO> buscarVeiculosPorMarca(String marcaId) throws Exception {
		final String VEICULOS = consultaFipeService.consultaVeiculosPorMarca(getTipoVeiculo(), marcaId);
		return Arrays.asList(JsonFactoryUtil.criarObjetos(VEICULOS, VeiculoModel.class)).stream()
				.sorted(Comparator.comparing(VeiculoModel::getName)).map(this::convert).collect(Collectors.toList());
	}

	public List<ModeloDTO> buscarModelosPorVeiculo(String marcaId, String veiculoId) throws Exception {
		final String MODELOS = consultaFipeService.consultaModeloPorVeiculo(getTipoVeiculo(), marcaId, veiculoId);
		return Arrays.asList(JsonFactoryUtil.criarObjetos(MODELOS, ModeloModel.class)).stream().map(this::convert)
				.sorted(Comparator.comparingInt(ModeloDTO::getAno)).collect(Collectors.toList());
	}

	public List<VeiculoPrecoDTO> consultaPrecos(String marcaId, String veiculoId) throws Exception {
		// todos os modelos
		final List<ModeloDTO> MODELOS = buscarModelosPorVeiculo(marcaId, veiculoId);
		List<VeiculoPrecoDTO> veiculoPrecoDTOs = new ArrayList<>();

		for(ModeloDTO modeloDTO : MODELOS){
		  // retorno um key estranho, diferente dos demais, e com isso, se chamarmso ocorrera erro interno na api externa
      // e na nossa também
		  if(modeloDTO.getKey().indexOf("-") > 4 ) continue;
      final String JSON = consultaFipeService.consultaPrecoVeiculo(getTipoVeiculo(), marcaId, veiculoId,
          modeloDTO.getKey());
      VeiculoPrecoDTO veiculoPrecoDTO = new VeiculoPrecoDTO();
      veiculoPrecoDTO.setAno(modeloDTO.getAno());
      veiculoPrecoDTO.setNome(modeloDTO.getModelo());
      veiculoPrecoDTO.setPreco(pegarPreco(JSON));
      if (!veiculoPrecoDTOs.isEmpty()) {
        setDiferencas(veiculoPrecoDTO, veiculoPrecoDTOs.get(veiculoPrecoDTOs.size() - 1));
      }
      veiculoPrecoDTOs.add(veiculoPrecoDTO);
    }

    /**
     * Ordenando para retornar do ano atual por primeiro
     */
		return veiculoPrecoDTOs.stream().sorted(Comparator.comparing(VeiculoPrecoDTO::getAno).reversed()).collect(
        Collectors.toList());
	}

	private MarcaDTO converter(MarcaModel marca) {
		return new MarcaDTO(marca.getName(), marca.getId());
	}

	private VeiculoDTO convert(VeiculoModel veiculo) {
		return new VeiculoDTO(veiculo.getId(), veiculo.getName());
	}

	private ModeloDTO convert(ModeloModel modelo) {
		return new ModeloDTO(modelo.getVeiculo(), Integer.parseInt(modelo.getKey().substring(0,4)), modelo.getKey());
	}

	private void setDiferencas(VeiculoPrecoDTO atual, VeiculoPrecoDTO anterior) {
		atual.setDiferencaReaisAnoAnterior(atual.getPreco() - anterior.getPreco());
		atual.setDiferencaPercentualAnoAnterior(new BigDecimal(atual.getDiferencaReaisAnoAnterior() / atual.getPreco() * 100)
				.setScale(2, RoundingMode.HALF_UP));
	}

	// TODO utilzar um REGEX
	private Double pegarPreco(final String JSON) throws Exception {
		String preco = JsonFactoryUtil.pegarPropriedade(JSON, "preco").replace("R$", "").replace(".", "")
				.replace(",", ".").trim();
		return Double.parseDouble(preco);
	}

}
