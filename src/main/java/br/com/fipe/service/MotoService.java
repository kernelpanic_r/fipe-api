package br.com.fipe.service;

import org.springframework.stereotype.Service;

import br.com.fipe.model.VeiculoEnum;

@Service
public class MotoService extends VeiculoService {

	@Override
	protected VeiculoEnum getTipoVeiculo() {
		return VeiculoEnum.MOTO;
	}

}
