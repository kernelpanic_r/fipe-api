package br.com.fipe.service;

import org.springframework.stereotype.Service;

import br.com.fipe.model.VeiculoEnum;

@Service
public class CarroService extends VeiculoService {

	@Override
	protected VeiculoEnum getTipoVeiculo() {
		return VeiculoEnum.CARRO;
	}

	

}
