package br.com.fipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiFipeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiFipeApplication.class, args);
	}

}
