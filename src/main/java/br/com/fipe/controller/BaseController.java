package br.com.fipe.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
public abstract class BaseController {

	protected final String JSON = "application/json";
}
