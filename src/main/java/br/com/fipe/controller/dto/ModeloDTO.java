package br.com.fipe.controller.dto;

public class ModeloDTO {

	private String modelo;
	private int ano;
	private String key;

	public ModeloDTO(String modelo, int ano, String key) {
		super();
		this.modelo = modelo;
		this.ano = ano;
		this.key = key;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
