package br.com.fipe.controller.dto;

public class TipoVeiculoDTO {

  private String nomeApi;
  private String nomeCompleto;

  public TipoVeiculoDTO(String nomeApi, String nomeCompleto) {
    this.nomeApi = nomeApi;
    this.nomeCompleto = nomeCompleto;
  }

  public String getNomeApi() {
    return nomeApi;
  }

  public void setNomeApi(String nomeApi) {
    this.nomeApi = nomeApi;
  }

  public String getNomeCompleto() {
    return nomeCompleto;
  }

  public void setNomeCompleto(String nomeCompleto) {
    this.nomeCompleto = nomeCompleto;
  }
}
