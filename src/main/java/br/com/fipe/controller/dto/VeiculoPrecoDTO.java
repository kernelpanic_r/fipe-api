package br.com.fipe.controller.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class VeiculoPrecoDTO {

	private String nome;
	private double preco;
	private BigDecimal diferencaPercentualAnoAnterior;
	private Double diferencaReaisAnoAnterior;
	private int ano;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public BigDecimal getDiferencaPercentualAnoAnterior() {
		return diferencaPercentualAnoAnterior;
	}

	public void setDiferencaPercentualAnoAnterior(BigDecimal diferencaPercentualAnoAnterior) {
		this.diferencaPercentualAnoAnterior = diferencaPercentualAnoAnterior;
	}

	public Double getDiferencaReaisAnoAnterior() {
		return diferencaReaisAnoAnterior;
	}

	public void setDiferencaReaisAnoAnterior(Double diferencaReaisAnoAnterior) {
		this.diferencaReaisAnoAnterior = diferencaReaisAnoAnterior;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	

}
