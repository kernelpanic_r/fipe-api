package br.com.fipe.controller;

import br.com.fipe.controller.dto.TipoVeiculoDTO;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fipe.model.VeiculoEnum;

@RestController
@RequestMapping(value = "/veiculos")
public class TipoVeiculoController extends BaseController {

	/**
	 * Retorna os tipos de veículos que estão disponíveis
	 * @return
	 */
	@GetMapping(produces = JSON)
	public ResponseEntity<List<TipoVeiculoDTO>> buscarTiposDeVeiculosDiponiveis() {
		return ResponseEntity.ok().body(Arrays.asList(VeiculoEnum.values()).stream()
				.map(this::convert).collect(Collectors.toList()));
	}

	/**
	 * Converte de Enum para DTO
	 * @param veiculoEnum
	 * @return
	 */
	private TipoVeiculoDTO convert(VeiculoEnum veiculoEnum){
		return new TipoVeiculoDTO(veiculoEnum.name().toLowerCase(), veiculoEnum.getDescricao());
	}

}
