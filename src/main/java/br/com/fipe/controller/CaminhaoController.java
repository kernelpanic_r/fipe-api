package br.com.fipe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fipe.service.CaminhaoService;
import br.com.fipe.service.VeiculoService;

@RestController
@RequestMapping(value = "/caminhoes")
public class CaminhaoController extends VeiculoController {

	@Autowired
	private CaminhaoService caminhaoService;
	
	@Override
	protected VeiculoService getClasseServico() {
		return caminhaoService;
	}

	
	
}
