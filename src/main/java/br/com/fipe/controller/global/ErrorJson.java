package br.com.fipe.controller.global;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorJson {

	private String mensagem;
	private LocalDateTime horario;
	
	@JsonProperty("status")
	private int httpStatus;
	
	
	public ErrorJson(String mensagem, LocalDateTime horario, int httpStatus) {
		super();
		this.mensagem = mensagem;
		this.horario = horario;
		this.httpStatus = httpStatus;
	}
	

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public LocalDateTime getHorario() {
		return horario;
	}

	public void setHorario(LocalDateTime horario) {
		this.horario = horario;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}


	
	
}
