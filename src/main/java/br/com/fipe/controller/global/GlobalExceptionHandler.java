package br.com.fipe.controller.global;

import br.com.fipe.exception.InternalErrorApiException;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.fipe.exception.TipoVeiculoNaoEncontradoException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = TipoVeiculoNaoEncontradoException.class)
	public ResponseEntity<ErrorJson> handleNotFound(TipoVeiculoNaoEncontradoException exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ErrorJson(exception.getMessage(), LocalDateTime.now(), HttpStatus.NOT_FOUND.value()));
	}
	
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ErrorJson> handleInternalError(Exception exception) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ErrorJson(exception.getMessage(), LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value()));
	}

	@ExceptionHandler(value = InternalErrorApiException.class)
	public ResponseEntity<ErrorJson> handleInternalError(InternalErrorApiException exception) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ErrorJson(exception.getMessage(), LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value()));
	}


}
