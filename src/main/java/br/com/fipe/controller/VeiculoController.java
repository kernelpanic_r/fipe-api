package br.com.fipe.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.fipe.controller.dto.MarcaDTO;
import br.com.fipe.controller.dto.ModeloDTO;
import br.com.fipe.controller.dto.VeiculoDTO;
import br.com.fipe.controller.dto.VeiculoPrecoDTO;
import br.com.fipe.service.VeiculoService;

public abstract class VeiculoController extends BaseController {

	/**
	 * Apenas será necessário a classe filha implementar este método, que todos os tipos
	 * de veículos terão estes métodos disponiveis
	 * @return
	 */
	protected abstract VeiculoService getClasseServico();
	
	
	@GetMapping(value = "/marcas")
	public ResponseEntity<List<MarcaDTO>> buscarMarcas() throws Exception {
		return ResponseEntity.ok(getClasseServico().buscarMarcas());
	}
	
	@GetMapping(value = "/marca/{marcaId}")
	public ResponseEntity<List<VeiculoDTO>> buscarVeiculosPorMarca(@PathVariable("marcaId") String marcaId) throws Exception{
		return ResponseEntity.ok(getClasseServico().buscarVeiculosPorMarca(marcaId));
	}

	@GetMapping(value = "/consulta/marca/{marcaId}/veiculo/{veiculoId}")
	public ResponseEntity<List<VeiculoPrecoDTO>> consultarValoresVeiculo(@PathVariable("marcaId") String marcaId, @PathVariable("veiculoId") String veiculoId) throws Exception{
		return ResponseEntity.ok(getClasseServico().consultaPrecos(marcaId, veiculoId));
	}
	
}
