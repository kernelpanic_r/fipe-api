package br.com.fipe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fipe.service.MotoService;
import br.com.fipe.service.VeiculoService;

@RestController
@RequestMapping(value = "/motos")
public class MotoController extends VeiculoController{

	@Autowired
	private MotoService motoService;
	
	@Override
	protected VeiculoService getClasseServico() {
		return motoService;
	}

}
