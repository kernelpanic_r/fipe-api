package br.com.fipe.exception;

public class TipoVeiculoNaoEncontradoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5389451663541021005L;

	public TipoVeiculoNaoEncontradoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipoVeiculoNaoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TipoVeiculoNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TipoVeiculoNaoEncontradoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TipoVeiculoNaoEncontradoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
