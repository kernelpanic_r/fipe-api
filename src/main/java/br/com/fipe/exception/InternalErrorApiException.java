package br.com.fipe.exception;

public class InternalErrorApiException extends  Exception {

  private static final long serialVersionUID = 1l;


  public InternalErrorApiException(String message) {
    super(message);
  }

}
