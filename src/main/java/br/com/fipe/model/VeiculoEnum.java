package br.com.fipe.model;

public enum VeiculoEnum {

	CARRO("carros"), MOTO("motos"), CAMINHAO("caminhoes");
	
	private String descricao;
	
	VeiculoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	
}
