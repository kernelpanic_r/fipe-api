package br.com.fipe.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonFactoryUtil {

	public static <E> E[] criarObjetos(final String JSON, Class<E> classe)
			throws JsonParseException, IOException, ClassNotFoundException {
		Class<E[]> arrayClass = (Class<E[]>) Class.forName("[L" + classe.getName() + ";");
		return  new ObjectMapper().readValue(JSON, arrayClass);
	}
	
	public static String pegarPropriedade(final String JSON, String propriedade) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(JSON, ObjectNode.class).get(propriedade).asText();
		
	}
	

}
