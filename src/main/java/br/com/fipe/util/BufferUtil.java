package br.com.fipe.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BufferUtil {

	public static String convertInputParaString(InputStream input) throws IOException {

		BufferedReader in = new BufferedReader(new InputStreamReader(input));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();

		return content.toString();

	}

}
